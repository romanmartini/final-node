const router = require('express').Router();
const { getAllClothes, getClothes, postClothes, putClothes, deleteClothes } = require('../controllers/clothes.controller');
const { validateRoles } = require('../middlewares/validate-auth');
const { validateIdParamsClothes, validatePutClothes, validatePostClothes } = require('../middlewares/validate-clothes');

router.get('/', getAllClothes);
router.get('/:_id', validateIdParamsClothes, getClothes);
router.post('/', [validateRoles('EMPLOYEE', 'ADMIN'), validatePostClothes], postClothes);
router.put('/:_id', [validateRoles('EMPLOYEE', 'ADMIN'), validatePutClothes], putClothes);
router.delete('/:_id', [validateRoles('EMPLOYEE', 'ADMIN'), validateIdParamsClothes], deleteClothes);

module.exports = router;