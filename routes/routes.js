const express = require('express');

const userRoutes = require("./user.routes");
const clothesRoutes = require("./clothes.routes");
const authRoutes = require("./auth.routes");
const { validateToken } = require('../middlewares/validate-auth');

const app = express();

app.use( express.json() ); 
app.use( validateToken )

app.use('/user', userRoutes);
app.use('/clothes', clothesRoutes);
app.use('/auth', authRoutes);

app.use( (req, res) =>  {
    return res.status(404).json({
        code: 'NOT-FOUND',
        success: false, 
        message: 'Resource not found',
        data: null
    })
})

module.exports = app;