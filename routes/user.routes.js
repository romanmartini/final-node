const router = require('express').Router();
const { getUser, putUser, deleteUser } = require('../controllers/user.controller');
const { validateOwnerOrAdminRole, validatePutUserRoleIsAdminRole } = require('../middlewares/validate-auth');
const { validateIdParamsUser, validatePutUser } = require('../middlewares/validate-user');

router.get('/:_id', [validateIdParamsUser, validateOwnerOrAdminRole], getUser);
router.put('/:_id', [validateIdParamsUser, validateOwnerOrAdminRole, validatePutUser, validatePutUserRoleIsAdminRole], putUser);
router.delete('/:_id', [validateIdParamsUser, validateOwnerOrAdminRole], deleteUser);

module.exports = router;