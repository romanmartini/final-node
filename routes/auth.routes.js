const router = require('express').Router();

const { signIn, signUp } = require('../controllers/auth.controller');
const { validateSignUp, validateSignIn } = require('../middlewares/validate-auth');

router.post('/signin', validateSignIn, signIn);
router.post('/signup', validateSignUp, signUp);

module.exports = router;