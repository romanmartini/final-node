const { Schema, model } = require('mongoose');

const userSchema = Schema(
    {
        name: {
            type: String,
            required: true
        },
        surname: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        email:{
            type: String,
            required: true,
            unique: true
        },
        role:{
            type: String,
            required: true,
            enum: ['ADMIN', 'EMPLOYEE', 'USER'],
            default: 'ADMIN'
        }
    },
    {
        timestamps: true    
    }
)

userSchema.methods.toJSON = function(){
    const {password, createdAt, updatedAt, __v, ...rest } = this.toObject();
    return rest;
}

module.exports = model('User', userSchema);