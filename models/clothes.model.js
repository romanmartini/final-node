const { Schema, model } = require('mongoose');

const clothesSchema = Schema(
    {
        category: {
            type: String,
            required: true,
            enum: ['diver', 't-shirt', 'jacket', 'pants']
        },
        stock: {
            type: Number,
            required: true
        },
        price: {
            type: Number,
            required: true
        },
        description: {
            type: String,
            required: true
        }
        
    },
    {
        timestamps: true    
    }
)

clothesSchema.methods.toJSON = function(){
    const { createdAt, updatedAt, __v, ...rest } = this.toObject();
    return rest;
}

module.exports = model('Clothes', clothesSchema);