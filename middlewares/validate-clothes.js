const Joi = require('joi')

const validateIdParamsClothes = async (req, res, next) => {

    const schema = Joi.object({
        _id: Joi.string().required()
    })

    try {
        await schema.validateAsync(req.params);
        return next(); 
    } catch (err) {
        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

const validatePutClothes = async (req, res, next) => {

    const schema = Joi.object({
        category: Joi.string().valid('diver', 't-shirt', 'jacket', 'pants').required(),
        stock: Joi.number().required(),
        price: Joi.number().required(),
        description: Joi.string().required()
    });

    try {
        await schema.validateAsync(req.body);
        return next(); 
    } catch (err) {
        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

const validatePostClothes = async (req, res, next) => {

    const schema = Joi.object({
        category: Joi.string().valid('diver', 't-shirt', 'jacket', 'pants').required(),
        stock: Joi.number().required(),
        price: Joi.number().required(),
        description: Joi.string().required()
    });

    try {
        await schema.validateAsync(req.body);
        return next(); 
    } catch (err) {
        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

module.exports = {
    validateIdParamsClothes,
    validatePutClothes,
    validatePostClothes
}