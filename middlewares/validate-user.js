const Joi = require('joi');

const validateIdParamsUser = async (req, res, next) => {
    const schema = Joi.object({
        _id: Joi.string().required()
    }).required();

    try {
        await schema.validateAsync(req.params);
        return next(); 
    } catch (error) {
        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

const validatePutUser = async (req, res, next) => {

    const schema = Joi.object({
        name: Joi.string().pattern(new RegExp('^[a-záéíóúü]+$', 'i')).required(),
        surname: Joi.string().pattern(new RegExp('^[a-záéíóúñü]+$', 'i')).required(),
        password: Joi.string().pattern(new RegExp('^[a-z0-9áéíóúñü@$!%*?&]{3,30}$', 'i')).required(),
        email: Joi.string().email().required(),
        role: Joi.string().valid('ADMIN', 'EMPLOYEE', 'USER').required()
    })

    try {

        await schema.validateAsync(req.body);
        return next(); 

    } catch (err) {

        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}



module.exports = {
    validateIdParamsUser,
    validatePutUser,
}