const jwt = require('jsonwebtoken');
const User = require('../models/user.model');
const Joi = require('joi')

const validateSignUp = async (req, res, next) => {

    const schema = Joi.object({
        name: Joi.string().pattern(new RegExp('^[a-záéíóúü]+$', 'i')).required(),
        surname: Joi.string().pattern(new RegExp('^[a-záéíóúñü]+$', 'i')).required(),
        password: Joi.string().pattern(new RegExp('^[a-z0-9áéíóúñü@$!%*?&]{3,30}$', 'i')).required(),
        email: Joi.string().email().required()
    })

    try {

        await schema.validateAsync(req.body);
        return next(); 

    } catch (err) {

        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

const validateSignIn = async (req, res, next) => {
    
    const schema = Joi.object({
        email: Joi.string().required(),
        password: Joi.string().required()
    });

    try {

        await schema.validateAsync(req.body);
        return next(); 

    } catch (err) {

        return res.status(400).json({
            code: 'VALIADATION-ERR',
            message: err.details[0].message,
            success: false,
            data: null
        });
    }
}

const validateToken = async (req, res, next) => {

    if( req.url === '/auth/signup' || req.url === '/auth/signin' ) return next();

    const authorization = req.headers.authorization;

    if ( !authorization ) {
        return res.status(401).json({
            code: 'AUTH-ERR',
            message: 'Token authorization must be provided',
            success: false,
            data: null
        })
    }

    try {

        const token = authorization.split(' ')[1];

        const { _id } = jwt.verify(token, process.env.PRIVATE_KEY);
        const user = await User.findById(_id);
        if ( !user ) {
            return res.status(401).json({
                code: 'AUTH-ERR',
                message: 'User does not exist',
                success: false,
                data: null
            })
        }
        
        req.user = user;
        return next();

    } catch (err) {
        console.log(err)
        return res.status(400).json({
            code: "AUTH-ERR",
            message: err.message,
            success: false,
            data: null
        })
    }
}

const validateRoles = (...roles) => {

    return async (req, res, next) => {

        if( !roles.includes(req.user.role) ){

            return res.status(403).json({
                code: 'AUTH-ERR',
                message: 'Resticted access',
                success: false,
                data: null
            })
        }

        return next();
    }
}

const validateOwnerOrAdminRole = (req, res, next) => {

    if( req.params._id === String(req.user._id) || req.user.role === 'ADMIN') return next();

    return res.status(403).json({
        code: 'AUTH-ERR',
        message: 'Resticted access',
        success: false,
        data: null
    })
}

const validatePutUserRoleIsAdminRole = async (req, res, next) => {

    if ( req.user.role !== req.body.role ){
        
        if( req.user.role !== 'ADMIN' ) {

            return res.status(403).json({
                code: 'AUTH-ERR',
                message: 'Resticted access. No permissions to update role',
                success: false,
                data: null
            })
        }
    } 
    next();
}

module.exports = {
    validateSignUp,
    validateSignIn,
    validateToken,
    validateRoles,
    validateOwnerOrAdminRole,
    validatePutUserRoleIsAdminRole
}