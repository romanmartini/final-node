require("dotenv").config();
const express = require('express');
const morgan = require("morgan");
const cors = require('cors');

const dbConnection = require('./config/mongoodb');
const routes = require('./routes/routes');

const app = express();

dbConnection();

app.use( morgan('tiny') );
app.use( cors() );
app.use( routes );

app.listen(process.env.PORT, () => {
    console.log(`Run port ${process.env.PORT}`)
});

const http = require('http');

console.log(http.STATUS_CODES)