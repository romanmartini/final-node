const Clothes = require('../models/clothes.model');

const getAllClothes = async (req, res) => {

    // const page = Number(req.query.page) || 1;
    // const limit = Number(req.query.limit) || 5;
    // const skipIndex = (page - 1) * limit;
    // const orderBy = req.query.orderby;

    try{

        // const count = await Clothes.countDocuments();
        const clothes = await Clothes.find()

            // .sort(orderBy)
            // .limit(limit)
            // .skip(skipIndex)
        // const pages = Math.ceil(count / limit)
        // return res.json({
        //     code: 'OK',
        //     message: null,
        //     success: true,
        //     data: {
        //         count,
        //         pages,
        //         clothes
        //     }
        // })

        return res.json({
            code: 'OK',
            message: null,
            success: true,
            data: clothes
        })

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        });
    }

}

const getClothes = async (req, res) => {

    try{
        const clothes = await Clothes.findById(req.params._id)
        if(!clothes){
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: 'Clothes does not exist',
                success: false,
                data: null 
            })
        }
        return res.json({
            code: 'OK',
            message: null,
            success: true,
            data: clothes
        })
    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        });
    }
}  

const postClothes = async (req, res) => {

    try{

        const clothes = await Clothes.create(req.body);

        return res.status(201).json({
            code: 'OK',
            message: 'Clothes created successfully',
            success: true,
            data: clothes 
        })

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null 
        })
    }

    

}  
const putClothes = async (req, res) => {

    try{

        const clothes = await Clothes.findOneAndUpdate({ _id: req.params._id }, { ...req.body }, { new: true, runValidators: true });
        if(!clothes){
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: 'Clothes does not exist',
                success: false,
                data: null 
            })
        }
        return res.status(201).json({
            code: 'OK',
            message: 'Clothes update successfully',
            success: true,
            data: clothes 
        })

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null 
        })
    }

}  

const deleteClothes = async (req, res) => {

    try{

        let clothes = await Clothes.deleteOne({ _id: req.params._id });
        if ( clothes.deletedCount === 0 ) {
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: 'Clothes does not exist',
                success: false,
                data: null 
            })
        }

        return res.json({
            code: 'OK',
            message:  'Clothes deleted successfully',
            success: true,
            data: null
        });

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        });
    }
}

module.exports = {
    getAllClothes,
    getClothes,
    postClothes,
    putClothes,
    deleteClothes
}