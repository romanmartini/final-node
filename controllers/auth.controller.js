const User = require('../models/user.model')
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');

const signIn = async (req, res) => {

    const { email, password } = req.body;

    try{

        const user = await User.findOne({ email });
        console.log(user)
        if( !user || !bcryptjs.compareSync(password, user.password) ) {
            return res.status(400).json({
                code: 'AUTH-ERR',
                message: 'Invalid email or password',
                success: false,
                data: null
            })
        }

        const token = jwt.sign({ _id: user._id }, process.env.PRIVATE_KEY, { expiresIn: '1h' });

        return res.json({
            code: "OK",
            message: null,
            success: true,
            data: {
                user,
                token
            }
        })

    }catch(err){
        return res.status(500).json({
            code: "ERR",
            message: err.message,
            success: false,
            data: null
        })
    }
}  

const signUp = async (req, res) => {

    const { password, ...rest } = req.body;

    rest.password = bcryptjs.hashSync(password, 10);

    try{

        const user = await User.create(rest);

        return res.status(201).json({
            code: 'OK',
            message: 'User created successfully',
            success: true,
            data: user 
        })

    }catch(err){
        
        return res.status(500).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        })
    }
}  


module.exports = {
    signIn,
    signUp
}