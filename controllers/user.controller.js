const User = require('../models/user.model');
const bcryptjs = require('bcryptjs');

const getUser = async (req, res) => {

    try{
        const user = await User.findById(req.params._id)
        if(!user){
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: null,
                success: false,
                data: null 
            })
        }
        return res.json({
            code: 'OK',
            message: null,
            success: true,
            data: user
        })
    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        });
    }
}  

const putUser = async (req, res) => {

    let { password, role, ...rest } = req.body;
    
    rest.password = bcryptjs.hashSync(password, 10); 

    try{

        if( req.user.role === 'ADMIN' ) rest.role = role;

        const user = await User.findOneAndUpdate({ _id: req.params._id }, { ...rest }, { new: true, runValidators: true });
        if(!user){
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: null,
                success: false,
                data: null 
            })
        }
        return res.status(201).json({
            code: 'OK',
            message: 'User update successfully',
            success: true,
            data: user 
        })

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null 
        })
    }

}  

const deleteUser = async (req, res) => {

    try{

        let user = await User.deleteOne({ _id: req.params._id });
        if ( user.deletedCount === 0 ) {
            return res.status(404).json({
                code: 'NOT-FOUND',
                message: 'Username does not exist',
                success: false,
                data: null 
            })
        }

        return res.json({
            code: 'OK',
            message: null,
            success: true,
            data: null
        });

    }catch(err){
        return res.status(400).json({
            code: 'ERR',
            message: err.message,
            success: false,
            data: null
        });
    }
}

module.exports = {
    getUser,
    putUser,
    deleteUser
}